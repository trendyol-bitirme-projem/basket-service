package com.trendyol.basketservice.service;

import com.trendyol.basketservice.entity.BasketEntity;
import com.trendyol.basketservice.model.BasketEntry;
import com.trendyol.basketservice.model.Notification;
import com.trendyol.basketservice.model.NotificationAction;
import com.trendyol.basketservice.model.ProductMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class KafkaService {

    public final BasketService basketService;
    @Value("${spring.kafka.topic.notification}")
    private String notificationDestination;

    private final KafkaTemplate<String, Notification> kafkaTemplate;


    public void sendNotifications(ProductMessage productMessage){
        List<BasketEntity> baskets = basketService.findBasketEntities();
        baskets = baskets.stream().filter(k -> {
            BasketEntry entry = k.getEntries().stream()
                    .filter(m -> m.getProductId().equals(productMessage.getProductId()) && m.getStockId().equals(productMessage.getStockId()))
                    .findFirst()
                    .orElse(null);
            return entry != null;
        }).collect(Collectors.toList());
        for (BasketEntity basket : baskets){
            Notification notification = new Notification();
            notification.setUserId(basket.getUserId());
            notification.setAction(NotificationAction.MAIL);
            notification.setType(productMessage.getType());
            notification.setProperties(productMessage.getProps());
            sendNotification(notification);
        }
    }

    public void sendNotification(Notification notification) {
        Message<Notification> kafkaMessage = MessageBuilder
                .withPayload(notification)
                .setHeader(KafkaHeaders.TOPIC, notificationDestination)
                .build();
        kafkaTemplate.send(kafkaMessage);
    }
}
