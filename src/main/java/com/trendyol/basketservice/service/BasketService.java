package com.trendyol.basketservice.service;

import com.trendyol.basketservice.entity.BasketEntity;
import com.trendyol.basketservice.integration.PriceRestService;
import com.trendyol.basketservice.integration.ProductRestService;
import com.trendyol.basketservice.model.*;
import com.trendyol.basketservice.model.price.Price;
import com.trendyol.basketservice.model.price.PriceQuery;
import com.trendyol.basketservice.model.price.ProductPrice;
import com.trendyol.basketservice.repository.BasketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class BasketService {
    public final BasketRepository basketRepository;
    public final ProductRestService productRestService;
    public final PriceRestService priceRestService;

    public List<BasketEntity> findBasketEntities(){
        return basketRepository.findAll();
    }

    public Basket getBasket(Integer userId) throws IOException {
        BasketEntity basketEntity = basketRepository.findByUserId(userId).orElse(new BasketEntity());
        List<Integer> productIds = basketEntity.getEntries().stream().map(BasketEntry::getProductId).collect(Collectors.toList());
        List<Product> products = productRestService.getProducts(productIds).execute().body();
        Price price = getPrice(basketEntity.getEntries());
        Basket basket = new Basket();
        List<ProductInfo> productInfos = new ArrayList<>();
        for (BasketEntry entry : basketEntity.getEntries()){
            Product product = products.stream()
                    .filter(k -> k.getId().equals(entry.getProductId()))
                    .findFirst()
                    .orElse(null);

            ProductPrice productPrice = price.getProductPrices().stream()
                    .filter(k-> k.getProductId().equals(entry.getProductId()) && k.getStockId().equals(entry.getStockId()))
                    .findFirst()
                    .orElse(null);

            if (product != null && productPrice != null){
                ProductInfo productInfo = new ProductInfo();
                productInfo.setId(entry.getProductId());
                productInfo.setStockId(entry.getStockId());
                productInfo.setName(product.getName());
                productInfo.setDescription(product.getDescription());
                Stock stock = product.getStocks().stream()
                        .filter(k -> k.getId().equals(entry.getStockId()))
                        .findFirst()
                        .orElse(null);
                if (stock != null){
                    productInfo.setImage(stock.getImage());
                }
                productInfo.setQuantity(entry.getQuantity());
                productInfo.setPrice(productPrice.getPrice());
                productInfo.setCurrency(productPrice.getCurrency());
                productInfos.add(productInfo);
            }
        }
        BasketInfo basketInfo = new BasketInfo();
        basketInfo.setCargo(price.getCargo());
        basketInfo.setSubTotal(price.getSubTotal());
        basketInfo.setTotal(price.getTotal());
        basketInfo.setCurrency(price.getCurrency());
        basketInfo.setTotalTax(price.getTotalTax());

        basket.setBasketInfo(basketInfo);
        basket.setProducts(productInfos);
        return basket;
    }

    public BasketEntity addToBasket(Integer userId, Integer productId, String stockId) throws Exception {
        isAvailableInStock(productId, stockId);
        BasketEntity basket = basketRepository.findByUserId(userId).orElse(new BasketEntity());
        basket.setUserId(userId);
        BasketEntry entry = basket.getEntries().stream()
                .filter(k -> k.getProductId().equals(productId) && k.getStockId().equals(stockId))
                .findFirst()
                .orElse(null);
        if (entry == null){
            BasketEntry newEntry = new BasketEntry();
            newEntry.setProductId(productId);
            newEntry.setStockId(stockId);
            newEntry.setQuantity(1);
            basket.getEntries().add(newEntry);
        }else {
            entry.setQuantity(entry.getQuantity()+1);
        }
        return basketRepository.save(basket);
    }


    public BasketEntity decreaseProductFromBasket(Integer userId, Integer productId, String stockId) throws Exception {
        BasketEntity basket = basketRepository.findByUserId(userId).orElse(new BasketEntity());
        basket.setUserId(userId);
        BasketEntry entry = basket.getEntries().stream()
                .filter(k -> k.getProductId().equals(productId) && k.getStockId().equals(stockId))
                .findFirst()
                .orElse(null);
        if (entry == null){
            throw new Exception("Product isn't found in the basket");
        }else {
            if(entry.getQuantity().equals(1)){
                basket.setEntries(basket.getEntries().stream()
                        .filter(k -> !k.getProductId().equals(productId) || !k.getStockId().equals(stockId))
                        .collect(Collectors.toList()));
            }else{
                entry.setQuantity(entry.getQuantity()-1);
            }
        }
        return basketRepository.save(basket);
    }

    public BasketEntity removeFromBasket(Integer userId, Integer productId, String stockId)  {
        BasketEntity basket = basketRepository.findByUserId(userId).orElse(new BasketEntity());
        basket.setUserId(userId);
        basket.setEntries(basket.getEntries().stream()
                .filter(k -> !k.getProductId().equals(productId) || !k.getStockId().equals(stockId))
                .collect(Collectors.toList()));
        return basketRepository.save(basket);
    }
    private void isAvailableInStock(Integer productId, String stockId) throws Exception {
        try{
            Product product = productRestService.getProduct(productId).execute().body();
            Stock stock = product.getStocks().stream()
                    .filter(k -> k.getId().equals(stockId))
                    .findFirst()
                    .orElse(null);
            if(stock != null && stock.getQuantity() > 0){
                return;
            }

        }catch (Exception exception){
            throw new Exception("Product isn't found");
        }
        throw new Exception("Product isn't available in the stock");
    }

    private Price getPrice(List<BasketEntry> entries) throws IOException {
        List<PriceQuery> priceQueries = new ArrayList<>();
        for(BasketEntry entry : entries){
            PriceQuery query = new PriceQuery();
            query.setProductId(entry.getProductId());
            query.setStockId(entry.getStockId());
            query.setQuantity(entry.getQuantity());
            priceQueries.add(query);
        }
        return priceRestService.getPrice(priceQueries).execute().body();
    }

}
