package com.trendyol.basketservice.queue;

import com.trendyol.basketservice.model.ProductMessage;
import com.trendyol.basketservice.service.KafkaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class KafkaListener {

    public final KafkaService kafkaService;

    @org.springframework.kafka.annotation.KafkaListener(
            topics = "${spring.kafka.topic.product-message}",
            groupId = "product-message-group",
            clientIdPrefix = "product-message-group",
            containerFactory = "productMessageQueueKafkaListenerContainerFactory"
    )
    public void listenProductMessage(ProductMessage productMessage){
        kafkaService.sendNotifications(productMessage);
    }
}
