package com.trendyol.basketservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Basket {
    private List<ProductInfo> products = new ArrayList<>();
    private BasketInfo basketInfo;

}
