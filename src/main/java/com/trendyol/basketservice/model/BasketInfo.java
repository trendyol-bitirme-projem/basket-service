package com.trendyol.basketservice.model;

import com.trendyol.basketservice.model.price.Currency;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketInfo {
    private Double subTotal;
    private Double totalTax;
    private Double cargo;
    private Double total;
    private Currency currency;
}
