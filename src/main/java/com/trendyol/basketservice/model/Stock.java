package com.trendyol.basketservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Stock {
    private String id;
    private String image;
    private String color;
    private String size;
    private Integer quantity;
}
