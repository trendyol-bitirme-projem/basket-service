package com.trendyol.basketservice.model.price;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceQuery {
    private Integer productId;
    private String stockId;
    private Integer quantity;
}