package com.trendyol.basketservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketEntry {
    private Integer productId;
    private String stockId;
    private Integer quantity;
}
