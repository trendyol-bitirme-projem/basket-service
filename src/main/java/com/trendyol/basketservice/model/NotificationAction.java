package com.trendyol.basketservice.model;

import lombok.Getter;

@Getter
public enum NotificationAction {
    MAIL
}
