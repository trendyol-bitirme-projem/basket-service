package com.trendyol.basketservice.model;

import com.trendyol.basketservice.model.price.Currency;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductInfo {
    private Integer id;
    private String stockId;
    private String name;
    private String description;
    private String image;
    private Integer quantity;
    private Double price;
    private Currency currency;
}
