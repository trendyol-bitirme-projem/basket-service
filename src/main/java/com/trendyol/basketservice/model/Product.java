package com.trendyol.basketservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Product {
    private Integer id;
    private String name;
    private String description;
    private String image;
    private String category;
    private Map<String, String> attributes = new HashMap<>();
    private List<String> information = new ArrayList<>();
    private Integer ownerId;
    private List<Stock> stocks = new ArrayList<>();
}