package com.trendyol.basketservice.integration;

import com.trendyol.basketservice.model.price.Price;
import com.trendyol.basketservice.model.price.PriceQuery;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;

public interface PriceRestService {
    @POST("/api/price/search")
    Call<Price> getPrice(@Body List<PriceQuery> queries);
}
