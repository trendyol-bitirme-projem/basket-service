package com.trendyol.basketservice.integration;

import com.trendyol.basketservice.model.Product;
import org.springframework.http.ResponseEntity;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

public interface ProductRestService {

    @GET("/api/product/list")
    Call<List<Product>> getProducts(@Query("ids") List<Integer> productIds);

    @GET("/api/product")
    Call<Product> getProduct(@Query("id") Integer productId);
}
