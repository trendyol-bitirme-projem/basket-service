package com.trendyol.basketservice.entity;

import com.trendyol.basketservice.model.BasketEntry;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Document("baskets")
public class BasketEntity {
    @Id
    private Integer id = Objects.hash(UUID.randomUUID().toString());
    private Integer userId;
    private List<BasketEntry> entries = new ArrayList<>();


}
