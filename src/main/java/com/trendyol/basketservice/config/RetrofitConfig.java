package com.trendyol.basketservice.config;

import com.trendyol.basketservice.integration.PriceRestService;
import com.trendyol.basketservice.integration.ProductRestService;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
public class RetrofitConfig {

    @Value("${product-service.url:#{null}}")
    private String productServiceBaseUrl;

    @Value("${price-service.url:#{null}}")
    private String priceServiceBaseUrl;

    @Bean
    public ProductRestService productRestService() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(10 * 60, TimeUnit.SECONDS);
        builder.connectTimeout(10 * 60, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        return new Retrofit.Builder()
                .baseUrl(productServiceBaseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ProductRestService.class);
    }

    @Bean
    public PriceRestService priceRestService() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(10 * 60, TimeUnit.SECONDS);
        builder.connectTimeout(10 * 60, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        return new Retrofit.Builder()
                .baseUrl(priceServiceBaseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(PriceRestService.class);
    }
}
