package com.trendyol.basketservice.controller;

import com.trendyol.basketservice.service.BasketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/basket")
@RequiredArgsConstructor
public class BasketController {
    public final BasketService basketService;

    @PostMapping("/add")
    public ResponseEntity<?> addToBasket(@RequestParam Integer userId,
                                         @RequestParam Integer productId,
                                         @RequestParam String stockId) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(basketService.addToBasket(userId, productId, stockId));
    }

    @DeleteMapping("/remove")
    public ResponseEntity<?> removeFromBasket(@RequestParam Integer userId,
                                              @RequestParam Integer productId,
                                              @RequestParam String stockId) {
        return ResponseEntity.status(HttpStatus.OK).body(basketService.removeFromBasket(userId, productId, stockId));
    }

    @PostMapping("/decrease")
    public ResponseEntity<?> decreaseProductFromBasket(@RequestParam Integer userId,
                                         @RequestParam Integer productId,
                                         @RequestParam String stockId) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(basketService.decreaseProductFromBasket(userId, productId, stockId));
    }

    @GetMapping
    public ResponseEntity<?> getBasket(@RequestParam(value = "userId") Integer userId) throws IOException {
        return ResponseEntity.status(HttpStatus.OK).body(basketService.getBasket(userId));
    }
}
