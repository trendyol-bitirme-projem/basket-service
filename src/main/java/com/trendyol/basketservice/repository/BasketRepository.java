package com.trendyol.basketservice.repository;


import com.trendyol.basketservice.entity.BasketEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BasketRepository extends MongoRepository<BasketEntity, Integer> {
    Optional<BasketEntity> findByUserId(Integer userId);

}

